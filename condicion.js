function preguntar() {
    let nombre = document.getElementById("nombre").value;
    let apellido = document.getElementById("apellido").value;
    let edad = parseInt(document.getElementById("edad").value);
    let licencia = document.getElementById("licencia").value;
    let fecha = document.getElementById("fecha").value;

    let anio        = fecha.substring(0,4);
    let mes         = fecha.substring(4,6);
    let dia         = fecha.substring(6,8);

    let fechaVencimiento        = new Date(anio, mes, dia);
    let hoy                     = new Date(); 



    // Condición 1: Ningún campo puede estar vacío
    if ( nombre != "" && apellido != "" && edad != NaN && licencia != "" && fecha != "") {
        // Condición 2: Ser mayor de 18
        if ( edad >= 18 ) {
            // Condición 3: Tener registro
            if ( licencia == "si" ) {
                // Condición 4: No debe estar caducado
                if ( hoy < fechaVencimiento ) {
                    alert(nombre + " " + apellido + ": Cumple todas las condiciones.");
                } else {
                    alert(nombre + " " + apellido + ": No cumple condición. Su licencia ha caducado.");
                }

            } else {
                alert(nombre + " " + apellido + ": No cumple condición. Debe poseer carnet de conducir.");
            }
        } else {
            alert(nombre + " " + apellido + ": No cumple condición. Debe ser mayor de edad.");
        }
    } else {
        alert(nombre + " " + apellido + ": No cumple condición. Todos los campos deben estar completos.");
    }



    
    
    
} 